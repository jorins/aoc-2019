#!/usr/bin/env pwsh

(Get-Content "1.in"  |
% {[Math]::Floor($_ / 3) - 2} |
Measure-Object -Sum).Sum
