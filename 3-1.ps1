#!/usr/bin/env pwsh

function Vectorise {
	param([string]$i)
	$dir = $i[0]
	$len = [int]$i.Substring(1)
	Switch ($dir) {
		"U" { @{x=0; y=-$len} }
		"D" { @{x=0; y= $len} }
		"L" { @{x=-$len; y=0} }
		"R" { @{x= $len; y=0} }
	}
}

function Get-Manhattan-Distance {
	param($a,$b)
	$o = [Math]::Abs($a.x - $b.x) + [Math]::Abs($a.y - $b.y)
	Write-Debug "Distance between ($($a.x), $($a.y)) and ($($b.x), $($b.y)) is $o"
	return $o
}

$wires = Get-Content "3.in" | % {
	# Oh my god
	, ($_.split(",") | % { Vectorise $_ })
}

$grid = @{}
$overlaps = @()
$wireIndex = 0
$wires | % {
	Write-Output "Working wire #$($wireIndex + 1)"
	$pos = @{x=0; y=0}

	$_ | % {
		$tar = @{x=$pos.x+$_.x; y=$pos.y+$_.y}
		#Write-Output "Got ($($_.x), $($_.y)) moving from ($($pos.x), $($pos.y)) to ($($tar.x), $($tar.y))"
		While (-not (($pos.x -eq $tar.x) -and ($pos.y -eq $tar.y))) {
			If ($grid[$pos.x] -eq $null) {
				$grid[$pos.x] = @{}
			}
			$grid[$pos.x][$pos.y] = $grid[$pos.x][$pos.y] -bor (1 -shl $wireIndex)
			If ($grid[$pos.x][$pos.y] -ne (1 -shl $wireIndex)) {
				$dist = Get-Manhattan-Distance @{x=0;y=0} $pos
				$overlaps += @{x=$pos.x; y=$pos.y; dist=$dist}
			}
			$pos.x += [math]::sign($_.x)
			$pos.y += [math]::sign($_.y)
		}
	}

	$wireIndex = $wireIndex + 1
}

Write-Output "Overlaps are:"
$overlaps | Format-Table
