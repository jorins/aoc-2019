#!/usr/bin/env pwsh

function Get-Fuel-For-Mass {
	Param ([float]$mass)
	[Math]::Floor($mass / 3) - 2
}

(Get-Content "1.in" | % {
	$sum = 0
	$add = Get-Fuel-For-Mass $_
	While ($add -gt 0) {
		$sum = $sum + $add
		$add = Get-Fuel-For-Mass $add
	}

	$sum
} | Measure-Object -Sum).Sum
