#!/usr/bin/env pwsh

$input = Get-Content "2.in" -Delimiter "," | % {[int]$_}  # Convert each to int
$expected = 19690720
$continue = $true

:outerer For ($noun = 0; $continue; $noun = $noun + 1) {
	:outer For ($verb = 0; $verb -le $noun; $verb = $verb + 1) {
		$in = $input.Clone()
		$in[1] = $noun
		$in[2] = $verb

		:inner For ($i = 0; $i -le $in.count; $i = $i + 4) {
			$op = $in[$i]
			$a = $in[$in[$i + 1]]
			$b = $in[$in[$i + 2]]
			$pos = $in[$i + 3]
			Write-Debug "$op // $a // $b // $pos "

			Switch ($op) {
				1 {
					$in[$pos] = $a + $b
				}
				2 {
					$in[$pos] = $a * $b
				}
				99 {
					Write-Debug "Opcode $op found, stopping"
					break inner
				}
				default {
					Write-Output "Unknown opcode: $op, stopping."
					break inner
				}
			}
		}

		$res = $in[0]
		if ($res -eq $expected) {
			Write-Output "Found answer:"
			$out = $noun * 100 + $verb
			Write-Output $out

			# Not sure why both are needed to break the loop instantly...
			$continue = $false
			break :outerer
		} else {
			Write-Output "Got $res from noun $noun, verb $verb (no match)"
		}
	}
}
