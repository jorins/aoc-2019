#!/usr/bin/env pwsh

function Vectorise {
	param([string]$i)
	$dir = $i[0]
	$len = [int]$i.Substring(1)
	Switch ($dir) {
		"U" { @{x=0; y=-$len} }
		"D" { @{x=0; y= $len} }
		"L" { @{x=-$len; y=0} }
		"R" { @{x= $len; y=0} }
	}
}

function Get-Manhattan-Distance {
	param($a,$b)
	$o = [Math]::Abs($a.x - $b.x) + [Math]::Abs($a.y - $b.y)
	Write-Debug "Distance between ($($a.x), $($a.y)) and ($($b.x), $($b.y)) is $o"
	return $o
}

$wires = Get-Content "3.in" | % {
	# Oh my god
	, ($_.split(",") | % { Vectorise $_ })
}

$grid = @{}
$overlaps = @()
$wireIndex = 0
$wires | % {
	Write-Output "Working wire #$($wireIndex + 1)"
	$pos = @{x=0; y=0}
	$stepSum = 0

	$_ | % {
		$tar = @{x=$pos.x+$_.x; y=$pos.y+$_.y}
		While (-not (($pos.x -eq $tar.x) -and ($pos.y -eq $tar.y))) {
			# Initialise grid
			If ($grid[$pos.x] -eq $null) { $grid[$pos.x] = @{} }

			# Doesn't override previous taken steps, if wire happens to reach
			# the same point twice.
			If ($grid[$pos.x][$pos.y] -eq $null) {
				$grid[$pos.x][$pos.y] = @{$wireIndex=$stepSum}
																			# Check for occupation of the other
			} ElseIf ($grid[$pos.x][$pos.y][[Math]::Abs($wireIndex-1)] -ne $null) {
				$steps = $grid[$pos.x][$pos.y][0] + $stepSum
				$overlaps += @{x=$pos.x; y=$pos.y; steps=$steps}
			}

			$pos.x += [math]::sign($_.x)
			$pos.y += [math]::sign($_.y)
			$stepSum += 1
		}
	}

	$wireIndex = $wireIndex + 1
}

Write-Output "Overlaps are:"
$overlaps | Format-Table
