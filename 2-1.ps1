#!/usr/bin/env pwsh

$in = Get-Content "2.in" -Delimiter "," | % {[int]$_}
$in[1] = 12
$in[2] = 2

$i = 0
:outer While ($true) {
	$op = $in[$i]
	$a = $in[$in[$i + 1]]
	$b = $in[$in[$i + 2]]
	$pos = $in[$i + 3]

	$i = $i + 4

	Write-Debug "$op // $a // $b // $pos "

	Switch ($op) {
		1 {
			$in[$pos] = $a + $b
		}
		2 {
			$in[$pos] = $a * $b
		}
		99 {
			Write-Debug "Opcode $op found, stopping"
			break outer
		}
		default {
			Write-Output "Unknown opcode: $op. Stopping."
			break outer
		}
	}
}

Write-Output $in[0]
